package ru.ibs.recruiterhappiness.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "technology")
public class Technology {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "technology_name")
    private String technology;
}

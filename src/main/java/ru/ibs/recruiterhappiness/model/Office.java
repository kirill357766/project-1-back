package ru.ibs.recruiterhappiness.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "offices")
public class Office {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "office")
    private String office;
}

package ru.ibs.recruiterhappiness.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "card_status")
public class CardStatus {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "status")
    private String status;
}

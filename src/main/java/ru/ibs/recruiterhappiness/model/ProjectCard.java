package ru.ibs.recruiterhappiness.model;

import lombok.Data;
import java.util.Date;
import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "project_card")
public class ProjectCard {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "project_name")
    private String projectName;
    @Column(name = "project_customer")
    private String projectCustomer;
    @Column(name = "fixed_price")
    private boolean fixedPrice;
    @Column(name = "time_material")
    private boolean timeAndMaterial;
    @Column(name = "MVP")
    private boolean MVP;
    @Column(name = "modernization")
    private boolean modernization;
    @Column(name = "functional_direction")
    private String functionalDirection;
    @Column(name = " subject_area")
    private String  subjectArea;
    @Column(name = "project_description")
    private String projectDescription;
    @Column(name = "project_tasks")
    private String projectTasks;
    @Column(name = "project_stage")
    private String projectStage;
    @Column(name = "deadline")
    private Date deadline;
    @OneToMany(cascade = CascadeType.MERGE)
    private List<Technology> technology;
    @Column(name = "stakeholders")
    private long stakeholders;
    @Column(name = "methodology")
    private String methodology;
    @Column(name = "product")
    private boolean product;
    @Column(name = "team_description")
    private String teamDescription;
    @Column(name = "location")
    private String location;
    @OneToOne(cascade = CascadeType.MERGE)
    private Office office;
    @Column(name = "schedule")
    private String schedule;
    @Column(name = "time_shift")
    private boolean timeShift;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "overtime")
    private String overtime;
    @Column(name = "start_procedure")
    private String startProcedure;
    @Column(name = "documenting")
    private boolean documenting;
    @Column(name = "standard")
    private boolean standard;
    @OneToOne(cascade = CascadeType.MERGE)
    private CardStatus cardStatus;





}

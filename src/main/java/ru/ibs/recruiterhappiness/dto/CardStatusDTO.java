package ru.ibs.recruiterhappiness.dto;

import lombok.*;

@Data
public class CardStatusDTO {
    private Long id;
    private String status;
}

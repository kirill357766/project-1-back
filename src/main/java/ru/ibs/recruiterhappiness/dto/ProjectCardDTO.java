package ru.ibs.recruiterhappiness.dto;

import lombok.Data;
import ru.ibs.recruiterhappiness.model.CardStatus;
import ru.ibs.recruiterhappiness.model.Office;
import ru.ibs.recruiterhappiness.model.Technology;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Data
public class ProjectCardDTO {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private Long id;
    private String projectName;
    private String projectCustomer;
    private boolean fixedPrice;
    private boolean timeAndMaterial;
    private boolean MVP;
    private boolean modernization;
    private String functionalDirection;
    private String  subjectArea;
    private String projectDescription;
    private String projectTasks;
    private String projectStage;
    private Date deadline;
    private List<TechnologyDTO> technologyDTOS;
    private long stakeholders;
    private String methodology;
    private boolean product;
    private String teamDescription;
    private String location;
    private OfficeDTO officeDTO;
    private String schedule;
    private boolean timeShift;
    private Date startDate;
    private String overtime;
    private String startProcedure;
    private boolean documenting;
    private boolean standard;
    private CardStatusDTO cardStatusDTO;
}

package ru.ibs.recruiterhappiness.dto;

import lombok.Data;

@Data
public class OfficeDTO {
    private Long id;
    private String office;
}

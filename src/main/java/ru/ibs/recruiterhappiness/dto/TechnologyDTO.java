package ru.ibs.recruiterhappiness.dto;

import lombok.Data;

import javax.persistence.Id;

@Data
public class TechnologyDTO {
    private Long id;
    private String technology;
}

package ru.ibs.recruiterhappiness.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ibs.recruiterhappiness.dto.CardStatusDTO;
import ru.ibs.recruiterhappiness.dto.TechnologyDTO;
import ru.ibs.recruiterhappiness.model.CardStatus;
import ru.ibs.recruiterhappiness.model.Technology;
import ru.ibs.recruiterhappiness.service.TechnologyService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Контроллер технологий")
@RequiredArgsConstructor
public class TechnologyController {

    @Autowired
    private TechnologyService technologyService;
    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation("Получение списка всех технологий")
    @GetMapping("/technologies")
    public List<TechnologyDTO> getAll() {
        List<Technology> technologies = technologyService.findAll();
        return technologies.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    @ApiOperation("Получение одной технологии")
    @GetMapping("/technologies/{id}")
    public TechnologyDTO getOne(@PathVariable("id") Long id) {
        if (id != null) {
            return convertToDTO(technologyService.findById(id));
        } else {
            throw new RuntimeException("error");
        }
    }

    @ApiOperation("Создание технологии")
    @PutMapping("/technology-create")
    public TechnologyDTO createTechnology(@RequestBody TechnologyDTO technologyDTO) {
        Technology technology = convertToEntity(technologyDTO);
        Technology technologyCreated = technologyService.saveTechnology(technology);
        return convertToDTO(technology);
    }

    @ApiOperation("Удаление технологии")
    @DeleteMapping("/technology-delete/{id}")
    public void deleteTechnology(@PathVariable Long id) {
        if (id != null) {
            technologyService.deleteById(id);
        } else {
            throw new RuntimeException("error");
        }
    }

    @ApiOperation("Обновление технологии")
    @PostMapping("technology-update/{id}")
    public void updateTechnology(@PathVariable("id") Long id, @RequestBody TechnologyDTO technologyDTO) {
        Technology technology = convertToEntity(technologyDTO);
        if (id != null) {
            technologyDTO.setId(id);
            technologyService.saveTechnology(technology);
        } else {
            throw new RuntimeException("error");
        }
    }

    private Technology convertToEntity(TechnologyDTO technologyDTO) {
        Technology technology = modelMapper.map(technologyDTO, Technology.class);
        return technology;
    }

    private TechnologyDTO convertToDTO(Technology technology) {
        TechnologyDTO technologyDTO = modelMapper.map(technology, TechnologyDTO.class);
        return technologyDTO;
    }
}
package ru.ibs.recruiterhappiness.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ibs.recruiterhappiness.dto.OfficeDTO;
import ru.ibs.recruiterhappiness.model.Office;
import ru.ibs.recruiterhappiness.service.OfficeService;

import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "Контроллер офисов")
@RestController
@RequiredArgsConstructor
public class OfficeController {

    @Autowired
    private OfficeService officeService;
    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation("Получение списка всех офисов")
    @GetMapping("/offices")
    public List<OfficeDTO> getAll() {
        List<Office> offices = officeService.findAll();
        return offices.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    @ApiOperation("Получение одного офиса")
    @GetMapping("/offices/{id}")
    public OfficeDTO getOne(@PathVariable Long id) {
        if (id != null) {
            return convertToDTO(officeService.findById(id));
        } else {
            throw new RuntimeException("error");
        }
    }

    @ApiOperation("Создание офиса")
    @PostMapping("/office-create")
    public OfficeDTO createOffice(@RequestBody OfficeDTO officeDTO) {
        Office office = convertToEntity(officeDTO);
        Office officeCreated = officeService.saveOffice(office);
        return convertToDTO(officeCreated);
    }

    @ApiOperation("Удаление офиса")
    @DeleteMapping("/office-delete/{id}")
    public void deleteOffice(@PathVariable Long id) {
        if (id != null) {
            officeService.deleteById(id);
        } else {
            throw new RuntimeException("error");
        }
    }

    @ApiOperation("Обновление офиса")
    @PutMapping("office-update/{id}")
    public void updateOffice(@PathVariable("id") Long id, @RequestBody OfficeDTO officeDTO) {
        Office office = convertToEntity(officeDTO);
        if (id != null) {
            officeDTO.setId(id);
            officeService.saveOffice(office);
        } else {
            throw new RuntimeException("error");
        }
    }

    private Office convertToEntity(OfficeDTO officeDTO) {
        Office office = modelMapper.map(officeDTO, Office.class);
        return office;
    }

    private OfficeDTO convertToDTO(Office office) {
        OfficeDTO officeDTO = modelMapper.map(office, OfficeDTO.class);
        return officeDTO;
    }
}

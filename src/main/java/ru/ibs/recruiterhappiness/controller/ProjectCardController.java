package ru.ibs.recruiterhappiness.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ibs.recruiterhappiness.dto.CardStatusDTO;
import ru.ibs.recruiterhappiness.dto.ProjectCardDTO;
import ru.ibs.recruiterhappiness.model.CardStatus;
import ru.ibs.recruiterhappiness.model.ProjectCard;
import ru.ibs.recruiterhappiness.service.CardStatusService;
import ru.ibs.recruiterhappiness.service.ProjectCardService;

import java.util.List;
import java.util.stream.Collectors;


@Api(tags = "Контроллер карточки проекта")
@RestController
@RequiredArgsConstructor
public class ProjectCardController {
    @Autowired
    private ProjectCardService projectCardService;
    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/cards")
    @ApiOperation("Получение списка всех карточек проекта")
    public List<ProjectCardDTO> getAll() {
        List<ProjectCard> projectCards = projectCardService.findAll();
        return projectCards.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    @ApiOperation("Получение одной карточки проекта")
    @GetMapping("/cards/{id}")
    public ProjectCardDTO getOne(@PathVariable("id") Long id) {
        if (id != null) {
            return convertToDTO(projectCardService.findById(id));
        } else {
            throw new RuntimeException("error");
        }
    }

    @ApiOperation("Создание карточки проекта")
    @PutMapping("/card-create")
    public ProjectCardDTO createProjectCard(@RequestBody ProjectCardDTO projectCardDTO) {
        ProjectCard projectCard =convertToEntity(projectCardDTO);
        ProjectCard projectCardCreated =  projectCardService.saveProjectCard(projectCard);
        return convertToDTO(projectCardCreated);
    }

    @ApiOperation("Удадение карточки проекта")
    @DeleteMapping("/card-delete/{id}")
    public void deleteProjectCard(@PathVariable Long id) {
        if (id != null) {
            projectCardService.deleteById(id);
        } else {
            throw new RuntimeException("error");
        }
    }

    @ApiOperation("Обновление карточки проекта")
    @PostMapping("card-update/{id}")
    public void updateProjectCard(@PathVariable("id") Long id, @RequestBody ProjectCardDTO projectCardDTO) {
        ProjectCard projectCard = convertToEntity(projectCardDTO);
        if (id != null) {
            projectCardDTO.setId(id);
            projectCardService.saveProjectCard(projectCard);
        } else {
            throw new RuntimeException("error");
        }
    }
    private ProjectCard convertToEntity(ProjectCardDTO projectCardDTO) {
        ProjectCard projectCard = modelMapper.map(projectCardDTO, ProjectCard.class);
        return projectCard;
    }

    private ProjectCardDTO convertToDTO(ProjectCard projectCard) {
        ProjectCardDTO projectCardDTO = modelMapper.map(projectCard, ProjectCardDTO.class);
        return projectCardDTO;
    }
}


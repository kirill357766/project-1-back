package ru.ibs.recruiterhappiness.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ibs.recruiterhappiness.dto.CardStatusDTO;
import ru.ibs.recruiterhappiness.model.CardStatus;
import ru.ibs.recruiterhappiness.service.CardStatusService;

import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "Контроллер статусов")
@RestController
@RequiredArgsConstructor
public class CardStatusController {

    @Autowired
    private CardStatusService cardStatusService;
    @Autowired
    private ModelMapper modelMapper;


    @ApiOperation("Получение списка всех статусов")
    @GetMapping("/statuses")
    public List<CardStatusDTO> getAll() {
        List<CardStatus> cardStatuses = cardStatusService.findAll();
        return cardStatuses.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    @ApiOperation("Получение одного статуса")
    @GetMapping("/statuses/{id}")
    public CardStatusDTO getOne(@PathVariable("id") Long id) {
        if (id != null) {
            return convertToDTO(cardStatusService.findById(id));
        } else {
            throw new RuntimeException("error");
        }
    }

    @ApiOperation("Создание статуса")
    @PutMapping("/status-create")
    public CardStatusDTO createCardStatus(@RequestBody CardStatusDTO cardStatusDTO) {
        CardStatus cardStatus = convertToEntity(cardStatusDTO);
        CardStatus cardStatusCreated = cardStatusService.saveCardStatus(cardStatus);
        return convertToDTO(cardStatusCreated);
    }

    @ApiOperation("Удаление статуса")
    @DeleteMapping("/status-delete/{id}")
    public void deleteStatus(@PathVariable Long id) {
        if (id != null) {
            cardStatusService.deleteById(id);
        } else {
            throw new RuntimeException("error");
        }
    }

    @ApiOperation("Обновление статуса")
    @PostMapping("status-update/{id}")
    public void updateStatus(@PathVariable("id") Long id, @RequestBody CardStatusDTO cardStatusDTO) {
        CardStatus cardStatus = convertToEntity(cardStatusDTO);
        if (id != null) {
            cardStatusDTO.setId(id);
            cardStatusService.saveCardStatus(cardStatus);
        } else {
            throw new RuntimeException("error");
        }
    }

    private CardStatus convertToEntity(CardStatusDTO cardStatusDTO) {
        CardStatus cardStatus = modelMapper.map(cardStatusDTO, CardStatus.class);
        return cardStatus;
    }

    private CardStatusDTO convertToDTO(CardStatus cardStatus) {
        CardStatusDTO cardStatusDTO = modelMapper.map(cardStatus, CardStatusDTO.class);
        return cardStatusDTO;
    }


}

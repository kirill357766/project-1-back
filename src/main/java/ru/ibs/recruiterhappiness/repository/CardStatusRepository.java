package ru.ibs.recruiterhappiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ibs.recruiterhappiness.model.CardStatus;

public interface CardStatusRepository extends JpaRepository<CardStatus, Long> {
}

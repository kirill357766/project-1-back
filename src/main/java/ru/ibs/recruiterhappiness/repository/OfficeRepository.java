package ru.ibs.recruiterhappiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ibs.recruiterhappiness.model.Office;

public interface OfficeRepository extends JpaRepository<Office, Long> {
}

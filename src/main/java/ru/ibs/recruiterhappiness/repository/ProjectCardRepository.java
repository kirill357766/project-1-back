package ru.ibs.recruiterhappiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ibs.recruiterhappiness.model.ProjectCard;

public interface ProjectCardRepository extends JpaRepository<ProjectCard, Long> {
}

package ru.ibs.recruiterhappiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ibs.recruiterhappiness.model.Technology;

public interface TechnologyRepository extends JpaRepository<Technology, Long> {
}

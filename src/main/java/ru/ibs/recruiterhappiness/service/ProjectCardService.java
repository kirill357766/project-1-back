package ru.ibs.recruiterhappiness.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ibs.recruiterhappiness.model.ProjectCard;
import ru.ibs.recruiterhappiness.repository.ProjectCardRepository;

import java.util.List;

@Service
public class ProjectCardService {
    private final ProjectCardRepository projectCardRepository;
    @Autowired
    public ProjectCardService(ProjectCardRepository projectCardRepository) {this.projectCardRepository = projectCardRepository;}

    public ProjectCard findById(Long id){return projectCardRepository.findById(id).orElse(null);}
    public List<ProjectCard> findAll(){return projectCardRepository.findAll();}
    public ProjectCard saveProjectCard(ProjectCard projectCard){return projectCardRepository.save(projectCard);}
    public void deleteById(Long id){projectCardRepository.deleteById(id);}
}

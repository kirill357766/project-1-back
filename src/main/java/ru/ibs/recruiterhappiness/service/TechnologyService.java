package ru.ibs.recruiterhappiness.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ibs.recruiterhappiness.model.Technology;
import ru.ibs.recruiterhappiness.repository.TechnologyRepository;

import java.util.List;
@Service
public class TechnologyService {
    private final TechnologyRepository technologyRepository;

    @Autowired
    public TechnologyService(TechnologyRepository technologyRepository){this.technologyRepository = technologyRepository;}

    public Technology findById(Long id){return technologyRepository.findById(id).orElse(null);}
    public List<Technology> findAll(){return technologyRepository.findAll();}
    public Technology saveTechnology(Technology technology){return technologyRepository.save(technology);}
    public void deleteById(Long id){technologyRepository.deleteById(id);}
}

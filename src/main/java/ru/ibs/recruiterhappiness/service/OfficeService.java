package ru.ibs.recruiterhappiness.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ibs.recruiterhappiness.model.Office;
import ru.ibs.recruiterhappiness.repository.OfficeRepository;

import java.util.List;
@Service
public class OfficeService {
    private final OfficeRepository officeRepository;

    @Autowired
    public OfficeService(OfficeRepository officeRepository){this.officeRepository = officeRepository;}

    public Office findById(Long id){return officeRepository.findById(id).orElse(null);}
    public List<Office> findAll(){return officeRepository.findAll();}
    public Office saveOffice(Office office){return officeRepository.save(office);}
    public void deleteById(Long id){officeRepository.deleteById(id);}
}

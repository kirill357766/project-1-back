package ru.ibs.recruiterhappiness.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ibs.recruiterhappiness.model.CardStatus;
import ru.ibs.recruiterhappiness.repository.CardStatusRepository;

import java.util.List;

@Service
public class CardStatusService {
    private final CardStatusRepository cardStatusRepository;

    @Autowired
    public CardStatusService(CardStatusRepository cardStatusRepository){this.cardStatusRepository = cardStatusRepository;}

    public CardStatus findById(Long id) throws RuntimeException{return cardStatusRepository.findById(id).orElse(null);}
    public List<CardStatus> findAll(){return cardStatusRepository.findAll();}
    public CardStatus saveCardStatus(CardStatus cardStatus){return cardStatusRepository.save(cardStatus);}
    public void deleteById(Long id){cardStatusRepository.deleteById(id);}
}
